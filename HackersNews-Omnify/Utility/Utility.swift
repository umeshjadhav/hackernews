//
//  Utility.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 25/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import UIKit

extension UIColor {
    
    static var themeOrange: UIColor {
       
        return UIColor(red: 248.0/255.0, green: 104.0/255.0, blue: 22.0/255.0, alpha: 1.0)
    }
}

extension Array {
    
    func getArrayElement<T>(from inputArray:[T], with index: Int, to position: Int) -> [T] {
       
        let arraySet = inputArray[index ..< position]
        
        let outputArray: [T] = Array<T>(arraySet)
       
        return outputArray
    }
}
