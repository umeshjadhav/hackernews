//
//  Constants.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 25/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//
//Sb = Storyboard
//Id = Identifier

import Foundation


struct Constant {

    //String constants
    static let No_Internet_Connection_Title_Msg = "Internet connection not available"
    static let No_Internet_Connection_Desc_Msg = "Turn on mobile data or use Wi-fi to access the Hacker News"

    //API URL's
    static let Get_News_Ids_Url = "https://hacker-news.firebaseio.com/v0/topstories.json?"
    static let Get_News_From_Id = "https://hacker-news.firebaseio.com/v0/item/"
    static let Get_Comments_for_Id = "https://hacker-news.firebaseio.com/v0/item/"

    //ViewControllers storyBoard identifiers
    static let LoginVC_Sb_Id = "loginVC"
    static let NewsVC_Sb_Id = "newsNVC"
    static let NewsDetailVC_Sb_Id = "newsDeatilVC"

    //Segue identifiers
    static let Comment_VC_Segue_Id = "toCommentVC"
    static let Article_VC_Segue_Id = "toArticleVC"

    //cell identifiers
    static let News_Cell_Id = "newsCell"
    static let Comment_Cell_Id = "commentCell"
}

