//
//  CommentModel.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 28/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import UIKit

struct CommentModel {

    var by: String
    var time: Int
    var comment: String
    
    init(by: String, time: Int, comment: String) {
        self.by = by
        self.time = time
        self.comment = comment
    }
    
}
