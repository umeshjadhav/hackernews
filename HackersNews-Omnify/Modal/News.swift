//
//  NewsModal.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 26/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import Foundation
import RealmSwift
import Realm

class News: Object {
    
    @objc dynamic var id: Int = 0
     @objc dynamic var title: String = ""
     @objc dynamic var time: Int = 0
     @objc dynamic var url: String? = ""
     var comments = List<Int>()
     @objc dynamic var score: Int = 0
     @objc dynamic var by: String = ""
    
    convenience init(id: Int, title: String, time: Int, url: String, comments: Int, score: Int, by: String) {
        self.init()
        self.id = id
        self.title = title
        self.time = time
        self.url = url
        self.comments.append(comments)
        self.score = score
        self.by = by
    }
}
