//
//  UJNewsCell.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 25/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import UIKit
import RealmSwift

class UJNewsCell: UITableViewCell {
    
    @IBOutlet weak var scoreLabel: UILabel!
    
    @IBOutlet weak var newsTitle: UILabel!
    
    @IBOutlet weak var linkLabel: UILabel!
    
    @IBOutlet weak var timeAndPostedByUserLabel: UILabel!
    
    @IBOutlet weak var commentCountLabel: UILabel!
    
    @IBOutlet weak var commentImage: UIImageView!

    var news: News! {
     
        didSet {
            scoreLabel.text = String(describing: news.score)
            newsTitle.text = news.title
            
            if let components = URLComponents(string: news.url!) {
                linkLabel.text = components.host
            }
           
            let time = TimeManager.timeAgoSinceDate(timeInterval: TimeInterval(exactly: news.time)!, currentDate: Date(), numericDates: true)

            timeAndPostedByUserLabel.text =  "\(time)" + " • " + "\(news.by)"
            commentCountLabel.text = "\(news.comments.count)"
            commentImage.image = UIImage(named: "comment")
        }
        
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
