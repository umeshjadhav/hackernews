//
//  CommentsCell.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 28/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import UIKit

class CommentsCell: UITableViewCell {

    @IBOutlet weak var dateAndUserNameLabel: UILabel!
    
    @IBOutlet weak var commentLabel: UILabel!

    var comment: CommentModel! {

        didSet {

            commentLabel.text = String(describing: comment.comment)
            let date = TimeManager.timestampToDate(timestamp: comment.time)
            dateAndUserNameLabel.text =  "\(date)" + " • " + "\(comment.by)"
        }

    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
