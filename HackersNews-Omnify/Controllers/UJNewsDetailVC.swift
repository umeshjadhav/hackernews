//
//  UJNewsDetailVC.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 27/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import UIKit

class UJNewsDetailVC: UIViewController {

    @IBOutlet weak var newsTitleLabel: UILabel!

    @IBOutlet weak var newsUrl: UILabel!

    @IBOutlet weak var newsTimeAndPostedByLabel: UILabel!

    @IBOutlet weak var commentButtonOutlet: UIButton!

    @IBOutlet weak var articleButtonOutlet: UIButton!

    @IBOutlet weak var commentHighlightView: UIView!

    @IBOutlet weak var articleHighlightView: UIView!

    @IBOutlet weak var commentButtonWidthConstant: NSLayoutConstraint!

    @IBOutlet weak var commentContainerView: UIView!
    
    @IBOutlet weak var articleContainerView: UIView!
    
    var newsDetail: News!

    override func viewDidLoad() {
        super.viewDidLoad()

        commentHighlightView.alpha = 1.0
        articleHighlightView.alpha = 0.0
        articleContainerView.isHidden = true
        setNewsDataOnView()
    }
    

    //MARK:- Methods
    func setNewsDataOnView() {

        if newsDetail != nil {

            newsTitleLabel.text = newsDetail.title

            let date = TimeManager.timestampToDate(timestamp: newsDetail.time)
            newsTimeAndPostedByLabel.text =  "\(date)" + " • " + "\(newsDetail.by)"

            commentButtonOutlet.setTitle("\(newsDetail.comments.count) COMMENTS", for: .normal)

            if newsDetail.url != "" {
                let components = URLComponents(string: newsDetail.url!)
                newsUrl.text = components!.host
                articleHighlightView.isHidden = false
                articleButtonOutlet.isHidden = false

            } else {

                newsUrl.text = ""
                //make comments button width bigger
                commentButtonWidthConstant.constant += self.view.frame.width - commentButtonWidthConstant.constant - 20
                 //hide article button
                articleHighlightView.isHidden = true
                articleButtonOutlet.isHidden = true


            }
        }
    }

    //MARK:- Actions
    @IBAction func commentButtonDidTapped(_ sender: UIButton) {

        commentHighlightView.alpha = 1.0
        articleHighlightView.alpha = 0.0
        articleContainerView.isHidden = true
        commentContainerView.isHidden = false
        
    }

    @IBAction func articleButtonDidTapped(_ sender: UIButton) {

        commentHighlightView.alpha = 0.0
        articleHighlightView.alpha = 1.0
        commentContainerView.isHidden = true
        articleContainerView.isHidden = false

    }

    //MARK:- Navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {

        if segue.identifier! == Constant.Comment_VC_Segue_Id {
           
            let commentsVC = segue.destination as? UJCommentVC
            commentsVC!.newsDetails = self.newsDetail
       
        }else {
            
            let articleVC = segue.destination as? UJArticleVC
            articleVC!.newsUrl = self.newsDetail.url
        
        }
        
    }

}
