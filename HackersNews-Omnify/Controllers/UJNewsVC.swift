//
//  UJNewsVC.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 25/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import UIKit
import RealmSwift
import Realm

class UJNewsVC: UIViewController {

    @IBOutlet weak var newsTableView: UITableView!

    @IBOutlet weak var lastUpadatedLabel: UILabel!
    
    var allNewsIds = [Int]()
    var newsIds = [Int]()
    
    var limit: Int?  //download limit
    var pageNo = 0  //page index

    var news: Results<News>?

    var isInternetConnected: Bool!
    var firstRecordId: Int?

    var lastUpadateTime: Double?

    var refreshControl = UIRefreshControl()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = .clear

        newsTableView.dataSource = self
        newsTableView.delegate = self

        isInternetConnected = Reachability.forInternetConnection().isReachable()
        self.dataHandler()
        setNewsUpdatedTime()
        configureRefreshControl()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(true)
       
    }
   
    //MARK:- Methods

    func configureRefreshControl() {

        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")

        refreshControl.addTarget(self,
                                 action: #selector(UJNewsVC.didPullRefreshControl),
                                 for: UIControl.Event.valueChanged)
        self.newsTableView.addSubview(refreshControl)
    }

    func setNewsUpdatedTime() {

        if UserDefaults.standard.value(forKey: "lastUpadateForNews") != nil {
            lastUpadateTime = UserDefaults.standard.value(forKey: "lastUpadateForNews") as? Double
            let lastUpadtedString = TimeManager.timeAgoSinceDate(timeInterval: TimeInterval(exactly: lastUpadateTime!)!, currentDate: Date(), numericDates: true)
            lastUpadatedLabel.text =  "Updated \(lastUpadtedString)"
        }else{
            lastUpadatedLabel.text = ""
        }
    }

    //This function is responsible for connectivity and data handling
    func dataHandler() {

        if isInternetConnected {

            //Internet connection is online
            if (self.getNewsFromDB() != nil) {

                //DB Have Data
                self.news = self.getNewsFromDB()
                firstRecordId = self.news?.first?.id
                getAllNewsIdsFromServer()

             }else {

                //Initiate the Download data from server
                getAllNewsIdsFromServer()

            }

        }else {

            //Internet connection is offline
            //Load data from realm db
            if (self.getNewsFromDB() != nil) {

                self.news = self.getNewsFromDB()
                self.newsTableView.reloadData()
                self.refreshControl.endRefreshing()

            }else{
                self.refreshControl.endRefreshing()
                //internet connection not available
                let noInternetAlert = UIAlertController(title: Constant.No_Internet_Connection_Title_Msg, message: Constant.No_Internet_Connection_Desc_Msg, preferredStyle: .alert)
                noInternetAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(noInternetAlert, animated: true)

            }
        }
    }

    func getAllNewsIdsFromServer() {

        WebserviceManager.getTopNewsIds(controller: self, from: Constant.Get_News_Ids_Url) { (response) in

            self.allNewsIds = (response as? [Int])!

            //check for is Latest news is available on server

            if self.firstRecordId == self.allNewsIds.first {

                //News update not availble

                print("No news Update available")

                //load from db
                let mainQueue = OperationQueue.main
                mainQueue.addOperation({
                    self.news = self.getNewsFromDB()
                    self.newsTableView.reloadData()
                    self.refreshControl.endRefreshing()
                })

            } else {

                print("News Update available")
                // send first ids with the set of 10 to download first 10 news
                self.resetTheApp()

                if self.allNewsIds.count > 10 {//if array is less than 10 dont slice array

                    self.newsIds = self.allNewsIds.getArrayElement(from: self.allNewsIds, with: self.pageNo, to: 10)
                    self.getNewsData(from: self.newsIds)

                }else{

                    self.getNewsData(from: self.allNewsIds)
                }
            }
        }
    }

    func getNewsData(from newsIds: [Int]) {

        //Store Api hit time
        let currentTimestamp = Date().timeIntervalSince1970
        UserDefaults.standard.set(currentTimestamp, forKey: "lastUpadateForNews")

        //Download news from Server
        WebserviceManager.downloadData(controller: self, from: Constant.Get_News_From_Id, with: newsIds) { (response) in

            //Store data into Realm for offline use.
            let mainQueue = OperationQueue.main
                mainQueue.addOperation({

                guard let newsArray = response as? NSArray else { return }
            
                for i in 0 ..< newsArray.count {
                
                    let news = News(id: ((newsArray[i] as AnyObject).value(forKey: "id")) as! Int,
                                    title: ((newsArray[i] as AnyObject).value(forKey: "title")) as! String,
                                    time: ((newsArray[i] as AnyObject).value(forKey: "time")) as! Int,
                                    url: (((newsArray[i] as AnyObject).value(forKey: "url")) as? String ?? ""),
                                    comments: 0,//((newsArray[i] as AnyObject).value(forKey: "Kids") as! Int)
                                    score: ((newsArray[i] as AnyObject).value(forKey: "score")) as! Int,
                                    by: ((newsArray[i] as AnyObject).value(forKey: "by")) as! String)
                    
                    if let commentsArray = ((newsArray[i] as AnyObject).value(forKey: "kids")) as? NSArray {

                        for j in 0 ..< commentsArray.count {
                             news.comments.append(commentsArray[j] as! Int)
                        }
                    }
                    let realm = try! Realm()
                    //Saving Data in realm db
                    try! realm.write {
                        realm.add(news)
                    }
                }
                //Load Data from Realm Db
                self.news = self.getNewsFromDB()
                self.newsTableView.reloadData()
                self.setNewsUpdatedTime()
                self.refreshControl.endRefreshing()
            })
        }
    }

    func getNewsFromDB() -> Results<News>? {
        let realm = try! Realm()
        let results: Results<News> =  realm.objects(News.self)
        return results
    }
    
    func deleteAllNewsFromRealm() {
        let realm = try! Realm()
        try! realm.write {
            realm.deleteAll()
        }

    }

    func resetTheApp() {
        self.pageNo = 0
        self.deleteAllNewsFromRealm()
    }

    //MARK:-  Acttions

    @objc func didPullRefreshControl() {
        self.dataHandler()
    }
}

//MARK:- Table View Datasource & Delegate

extension UJNewsVC: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if news?.count != nil {
            return news!.count
        }else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: Constant.News_Cell_Id) as! UJNewsCell
        let newNews = news![indexPath.row] as News
        cell.news = newNews
        return cell
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 103.0
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if isInternetConnected {
            //Pagignation
            if indexPath.row == news!.count - 1 {

                pageNo += 1

                let pageIndex = pageNo * 10

                if pageIndex < allNewsIds.count {

                    limit =  pageIndex + 10

                    if limit! > allNewsIds.count {

                        limit = allNewsIds.count

                    }
                    self.newsIds = self.allNewsIds.getArrayElement(from: self.allNewsIds,
                                                                   with: pageIndex,
                                                                   to: self.limit!)
                    self.getNewsData(from: self.newsIds)
                }

            }
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let newsDetailVC = UIStoryboard.init(name: "Main", bundle: nil).instantiateViewController(withIdentifier: Constant.NewsDetailVC_Sb_Id) as! UJNewsDetailVC
        newsDetailVC.newsDetail = self.news![indexPath.row]
        self.navigationController?.pushViewController(newsDetailVC, animated: true)
        self.newsTableView.deselectRow(at: indexPath, animated: true)
    }
}
