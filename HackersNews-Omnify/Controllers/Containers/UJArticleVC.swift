//
//  UJArticleVC.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 27/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import UIKit
import WebKit

class UJArticleVC: UIViewController {

    var newsUrl: String?

    @IBOutlet weak var webView: WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if newsUrl != nil && newsUrl != "" {
            webView.load(URLRequest(url: URL(string: newsUrl!)!))
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

}
