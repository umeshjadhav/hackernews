//
//  UJCommentVC.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 27/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import UIKit

class UJCommentVC: UIViewController {

    var newsDetails: News?
    
    var limit: Int?  //download limit
    var pageNo = 0  //page index
    
    var comments: [CommentModel]? = []
    
    var isInternetConnected: Bool!

    var allCommentsIds = [Int]()

    var commentIds = [Int]()
    
    @IBOutlet weak var commentTableView: UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()

        isInternetConnected = Reachability.forInternetConnection().isReachable()
        self.commentTableView.dataSource = self
        self.commentTableView.delegate = self
        self.commentTableView.rowHeight = UITableView.automaticDimension
        self.commentTableView.estimatedRowHeight = 80
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if self.newsDetails != nil {

            for (_, value) in self.newsDetails!.comments.enumerated() {

                allCommentsIds.append(value)
            }

            retriveTenCommentsIdsFromArray()

        }
    }

    //MARK:- Mehods

    func retriveTenCommentsIdsFromArray() {

        if allCommentsIds.count > 10 {//if array is less than 10 dont slice array

            self.commentIds = allCommentsIds.getArrayElement(from: allCommentsIds, with: self.pageNo, to: 10)
            self.getCommentsData(from: self.commentIds)

        } else {

            self.getCommentsData(from: self.allCommentsIds)
        }
    }

    func getCommentsData(from commentsIds: [Int]) {

        //Download comments from Server
        WebserviceManager.downloadData(controller: self, from: Constant.Get_Comments_for_Id, with: commentsIds) { (response) in

            guard let commentsArray = response as? NSArray else { return }

            for i in 0 ..< commentsArray.count {

                if commentsArray[i] is NSNull {
                //skip null element
                }else {

                    let comment = CommentModel(by: ((commentsArray[i] as AnyObject).value(forKey:"by") as? String ?? ""),
                                           time: ((commentsArray[i] as AnyObject).value(forKey:"time") as? Int)!,
                                           comment: ((commentsArray[i] as AnyObject).value(forKey:"text") as? String ?? "Comment deleted"))

                    self.comments!.append(comment)
                }
            }

            DispatchQueue.main.async {
                self.commentTableView.reloadData()
            }
        }
    }
}

extension UJCommentVC: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if comments?.count != nil {
            return comments!.count
        }else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell =  tableView.dequeueReusableCell(withIdentifier: Constant.Comment_Cell_Id) as! CommentsCell
        let comment = comments![indexPath.row] as CommentModel
        cell.comment = comment
        return cell
    }

    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        if isInternetConnected {
            //Pagignation
            if indexPath.row == comments!.count - 1 {

                pageNo += 1

                let pageIndex = pageNo * 10

                if pageIndex < allCommentsIds.count {

                    limit =  pageIndex + 10

                    if limit! > allCommentsIds.count {

                        limit = allCommentsIds.count

                    }
                    self.commentIds = self.allCommentsIds.getArrayElement(from: self.allCommentsIds,
                                                                   with: pageIndex,
                                                                   to: self.limit!)
                    self.getCommentsData(from: self.commentIds)
                }
            }
        }
    }
}
