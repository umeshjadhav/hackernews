//
//  UJLoginVC.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 25/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import UIKit
import GoogleSignIn
import FirebaseAuth
import Firebase

class UJLoginVC: UIViewController {

    
    @IBOutlet weak var googleSignInButton: GIDSignInButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        configureGoogleSignIn()
    
        //Styling Google sign in button
        googleSignInButton.style = .wide
        googleSignInButton.colorScheme = .light

    }

    //MARK:- Handline Actions
    
    @IBAction func googleSignInButtonDidTapped(_ sender: GIDSignInButton) {
        
         GIDSignIn.sharedInstance().signIn()
    }
    
    //MARK:- Helper methods
    
    func configureGoogleSignIn() {
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
    }

}

extension UJLoginVC: GIDSignInUIDelegate, GIDSignInDelegate {
    
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL,
                     options: [UIApplication.OpenURLOptionsKey : Any]) -> Bool {
        
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication:options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
                                                 annotation: [:])
    }
    
    func application(_ application: UIApplication, open url: URL,
                     sourceApplication: String?, annotation: Any) -> Bool {
        
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
       
        if error != nil {
           
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                                       accessToken: authentication.accessToken)
      
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if error != nil {
              
                return
            }
            
            //Successfull Login
            let mainStoryboard = UIStoryboard.init(name: "Main", bundle: nil)
            let newsVc = mainStoryboard.instantiateViewController(withIdentifier: Constant.NewsVC_Sb_Id)
            self.present(newsVc, animated: false, completion: nil)
            
        }
     
    }
     
}
