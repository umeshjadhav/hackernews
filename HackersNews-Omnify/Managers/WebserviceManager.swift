//
//  WebserviceManager.swift
//  HackersNews-Omnify
//
//  Created by Umesh on 25/09/18.
//  Copyright © 2018 Omnify. All rights reserved.
//

import UIKit
import Foundation

class WebserviceManager: NSObject {

    class func getTopNewsIds(controller: UIViewController,
                             from urlString: String,
                             callBack:@escaping (Any) -> Void) {

        guard let requestUrl = URL(string:urlString) else { return }
        let request = URLRequest(url:requestUrl)

        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in

            guard let data = data, error == nil else { return }

                //JSONSerialization
                do {
                    let jsonArray = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSArray
                    callBack(jsonArray)
                } catch let error as NSError {
                    print(error)
                }
        }
        task.resume()
    }

    class func downloadData(controller: UIViewController, from urlString: String, with ids: [Int], callBack:@escaping (Any) -> Void) {

        var activityIndicator: UIActivityIndicatorView!
        DispatchQueue.main.async {
            activityIndicator = UIActivityIndicatorView.init(style: .gray)
            activityIndicator.startAnimating()
            controller.view.addSubview(activityIndicator)
            activityIndicator.center = controller.view.center
            activityIndicator.hidesWhenStopped = false
        }

        var newsArray = [Any]()
       
        let downloadQueue = OperationQueue()
        downloadQueue.maxConcurrentOperationCount = 1
        downloadQueue.name = "DownloadQueue"
        
        var downloadOperation =  BlockOperation()
        
        for i in 0..<ids.count {
            
             downloadOperation = BlockOperation {
                
                let url = URL.init(string: urlString + "\(ids[i])" + ".json")
                
                WebserviceManager.downloadManager(downloadUrl: url!) { (response) in
                    
                    newsArray.append(response)

                    if ids[i] == ids.last {
                         DispatchQueue.main.async {
                            activityIndicator.stopAnimating()
                            activityIndicator.removeFromSuperview()
                         }
                          callBack(newsArray)
                    }
                }
            }
            downloadQueue.addOperations([downloadOperation], waitUntilFinished: true)
        }
    }
    
    
    class func downloadManager(downloadUrl: URL, callBack:@escaping (Any) -> Void) {
        
        let request = URLRequest(url:downloadUrl)
        
        let task = URLSession.shared.dataTask(with: request) {(data, response, error) in
            
            guard let data = data, error == nil else { return }
            
            //JSONSerialization
            do {
                let jsonObject = try JSONSerialization.jsonObject(with: data, options: .allowFragments)
                
                callBack(jsonObject)
            } catch let error as NSError {
                print(error)
            }
        }
        task.resume()
    }
}




